import React, {Component} from 'react';
import $ from 'jquery';
import Header from './components/Header';
import Slide from './components/Slide';
import Banner3col from './components/Banner3col';
import TabProduct from './components/TabProduct';
import Hotdeal from './components/Hotdeal';
import './assets/style/Other.scss';
//import Control from './components/Control';
//import Form from './components/Form';
//import List from './components/List';
//import './App.css';
//import tasks from './data/task.js';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            
        };  
    }

    render() {
        return(
            <div className="pageTop">
                <Header />
                <Slide />
                <div className="c-support">
                    <div className="container">
                        <div className="row">
                            <div className="c-support__item col-md-3 col-sm-6">
                                <div className="c-support__icon"><i className="fa fa-truck"></i></div>
                                <h3 className="c-support__text">
                                    FREE SHIPPING<span>For All Orders Over $220</span>
                                </h3>
                            </div>
                            <div className="c-support__item col-md-3 col-sm-6">
                                <div className="c-support__icon"><i className="fa fa-retweet"></i></div>
                                <h3 className="c-support__text">
                                    30 DAYS RETURNS<span>Money Back Guarantee</span>
                                </h3>
                            </div>
                            <div className="c-support__item col-md-3 col-sm-6">
                                <div className="c-support__icon"><i className="fa fa-gift"></i></div>
                                <h3 className="c-support__text">
                                    PROMOTIONS<span>10% Member Discount</span>
                                </h3>
                            </div>
                            <div className="c-support__item col-md-3 col-sm-6">
                                <div className="c-support__icon"><i className="fa fa-volume-control-phone"></i></div>
                                <h3 className="c-support__text">
                                    ONLINE SUPPORT<span>Free support 24/7 Per Week</span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <Banner3col />          
                <TabProduct />
                <Hotdeal />
                
            </div>
        );
    }
}

export default App;

