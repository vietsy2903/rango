import React, {Component} from 'react';
import '../assets/style/Header.scss';
import Logo from '../assets/image/logo.png';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };  
    }

    componentDidMount() {
    	window.addEventListener('scroll', function () {
    		var htop = $('.c-header__top').height();
	        var scroll = $(window).scrollTop();
	        if (scroll > htop) {
		        $('.c-header__inner').css({top: 0, padding: '5px 0'});
		    } else{
		    	$('.c-header__inner').css({top: htop, padding: '20px 0'});
		    }
    	});        
    }

    render() {
        return(
            <header className="c-header">
            	<div className="c-header__top">
            		<div className="container">
	            		<div className="row justify-content-between">
	            			<div  className="c-header__nav">
			            		<ul>
			            			<li><a href="#"><i className="fa fa-gift"></i>Gift Voucher</a></li>
			            			<li><a href="#"><i className="fa fa-life-ring"></i>Help & Advice</a></li>
			            		</ul>
		            		</div>
		            		<div className="c-header__nav">
			            		<ul>
			            			<li className="is-login">
			            				<a href="#">Login / Sign up</a>
			            				<div className="c-header__login">
					            			<form action="true">
					            				<div className="input-group mb-4">
													<div className="input-group-prepend">
														<span className="input-group-text">Username</span>
													</div>
													<input type="text" className="form-control" />
												</div>
												<div className="input-group mb-4">
													<div className="input-group-prepend">
														<span className="input-group-text">Password</span>
													</div>
													<input type="password" className="form-control" />
												</div>
												<div className="d-flex justify-content-between align-items-center">
													<button type="submit" className="btn btn-info">Login</button>
													<a href="#">Forgot Your Password?</a>
												</div>
					            			</form>
					            		</div>
			            			</li>
			            			<li><a href="#"><i className="fa fa-heart"></i>Wishlist (0)</a></li>
			            		</ul>
			            		
		            		</div>
	            		</div>
            		</div>
            	</div>
            	<div className="c-header__inner">
            		<div className="container">
	            		<div className="row justify-content-between">
	            			<h1 className="c-header__logo"><a href="/"><img src={Logo} alt="Logo" /></a></h1>
	            			<ul className="c-header__menu">
	            				<li>
	            					<a className="is-hover" href="#">home</a>
	            					<ul className="c-header__sub">
	            						<li><a href="#">Sub menu 1</a></li>
	            						<li><a href="#">Sub menu 2</a></li>
	            						<li><a href="#">Sub menu 3</a></li>
	            						<li><a href="#">Sub menu 4</a></li>
	            					</ul>
	            				</li>
	            				<li><a className="is-hover" href="#">shop</a></li>
	            				<li><a className="is-hover" href="#">blog</a></li>
	            				<li><a className="is-hover" href="#">page</a></li>
	            				<li><a className="is-hover" href="#">vendor</a></li>
	            				<li><a className="is-hover" href="#">portfolios</a></li>
	            			</ul>
	            			<div className="c-header__shop">
	            				<div className="c-header__btnSearch"><i className="fa fa-search"></i></div>
	            				<div className="c-header__cart"><span><i className="fa fa-shopping-basket"></i></span><sup>0</sup></div>
	            			</div>
	            		</div>
            		</div>
            	</div>
            	<div className="c-search">
            		<input className="form-control form-control-lg" type="text" placeholder="Search..." />
            	</div>
            </header>
        );
    }
}

export default Header;