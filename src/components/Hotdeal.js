import React, {Component} from 'react';

class Hotdeal extends Component {
    constructor(props) {
        super(props);

        this.state = {
        	isdate: 'March 15, 2020',
        	days: 0,
        	hours: 0,
        	mins: 0,
        	secs: 0
        };  
    }

    componentDidMount(){
		setInterval(() => this.getTime(this.state.isdate, 1000));
    }

	getTime(){
		const time = Date.parse(this.state.isdate) - Date.parse(new Date());
		const secs = Math.floor((time/1000)%60);
		const mins = Math.floor((time/1000/60)%60);
		const hours = Math.floor((time/1000/60/60)%24);
		const days = Math.floor((time/1000/60/60/24));
		this.setState({days, hours, mins, secs});
	}

    render() {
        return(
        	<div className="c-hotdeal">
                <div className="container">
                    <div className="c-hotdeal__inner row">
                        <div className="c-hotdeal__item c-hotdeal__item--left">
                        	<img src={process.env.PUBLIC_URL + '/image/hotdeal01.png'} alt="" />
                        	<div className="c-hotdeal__sell">-50%</div>
                        	<h3 className="c-hotdeal__ttl02"><span>MEN’S FASHION</span></h3>
                        </div>
                        <div className="c-hotdeal__box">
		                    <h3 className="c-hotdeal__ttl01"><span>Hot deals</span>deals of weeks</h3>
		                    <p className="c-hotdeal__txt01">Donec nec justo eget felis facilisis fermentum.<br/>Aliquam porttitor mauris.</p>
		                    <div className="c-hotdeal__time">
		                        <ul>
		                            <li>{this.state.days}<span>days</span></li>
		                            <li>{this.state.hours}<span>hours</span></li>
		                            <li>{this.state.mins}<span>mins</span></li>
		                            <li>{this.state.secs}<span>secs</span></li>
		                        </ul>
		                    </div>
		                    <a href="#" className="c-hotdeal__btn"><span>view all hot deal</span></a>
		                </div>
		                <div className="c-hotdeal__item c-hotdeal__item--right">
		                	<img src={process.env.PUBLIC_URL + '/image/hotdeal02.png'} alt="" />
		                	<h3 className="c-hotdeal__ttl02"><span>WOMEN'S FASHION</span></h3>
		                	<div className="c-hotdeal__sell">-70%</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Hotdeal;