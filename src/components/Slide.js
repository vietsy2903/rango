import React, {Component} from 'react';
import Slider from 'react-slick';
import SliderItem from './SlideItem';
import '../assets/style/Slide.scss';
import Arrow from '../assets/image/arrow.png';

class Slide extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sliderItem: [
                {
                    img: 'https://via.placeholder.com/1920x800',
                    title: 'Slider 01',
                    share: 'sale up to 70% off',
                    content: 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.',
                    url: '#'
                },{
                    img: 'https://via.placeholder.com/1920x800',
                    title: 'Slider 02',
                    share: 'sale up to 60% off',
                    content: 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.',
                    url: '#'
                },{
                    img: 'https://via.placeholder.com/1920x800',
                    title: 'Slider 03',
                    share: 'sale up to 50% off',
                    content: 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.',
                    url: '#'
                },{
                    img: 'https://via.placeholder.com/1920x800',
                    title: 'Slider 04',
                    share: 'sale up to 40% off',
                    content: 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.',
                    url: '#'
                }
            ],
        };

        this.Next = this.Next.bind(this);
        this.Prev = this.Prev.bind(this);
    }
    Next() {
        this.slider.slickNext();
    }
    Prev() {
        this.slider.slickPrev();
    }

    render() {
        const settings = {
            dots: false,
            arrows: false,
            infinite: true,
            //fade: true,
            autoplay: true,
            speed: 500,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
            cssEase: 'linear',
            focusOnSelect: false,
        };
        
        const sliderItems = this.state.sliderItem;
        const elmSlideItems = sliderItems.map((sliderItem, index) =>{
            return(
                <SliderItem key={index} sliderItem={sliderItem} />
            );
        });

        return(  
            <div className="c-slider">                    
                <Slider className="c-slider__wrap" ref={c => (this.slider = c)} {...settings}>
                    {elmSlideItems}
                </Slider>
                <div className="c-slider__control">
                    <div className="c-slider__prev" onClick={this.Prev}><img src={Arrow} alt="Arrow Prev" /></div>
                    <div className="c-slider__next" onClick={this.Next}><img src={Arrow} alt="Arrow Next" /></div>
                </div>
            </div>
        );
    }
}

export default Slide;