import React, {Component} from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Product from './Product';
//import 'react-tabs/style/react-tabs.scss';

class TabProduct extends Component {
	constructor(props) {
        super(props);

        this.state = {
            product01: [
                {
                    img: '/image/product01.jpg',
                    title: 'amed-sleeve tops group',
                    price: '$60',
                    star: 1
                },{
                    img: '/image/product02.jpg',
                    title: 'Avitae lectus default',
                    price: '$61',
                    star: 5
                },{
                    img: '/image/product03.jpg',
                    title: 'Azrouel dress variable',
                    price: '$65',
                    star: 5
                },{
                    img: '/image/product04.jpg',
                    title: 'Azrouel dress variable',
                    price: '$100',
                    star: 5
                }
            ],
            product02: [
                {
                    img: '/image/product05.jpg',
                    title: 'amed-sleeve tops group',
                    price: '$60',
                    star: 3
                },{
                    img: '/image/product06.jpg',
                    title: 'Avitae lectus default',
                    price: '$61',
                    star: 5
                },{
                    img: '/image/product07.jpg',
                    title: 'Azrouel dress variable',
                    price: '$65',
                    star: 5
                },{
                    img: '/image/product08.jpg',
                    title: 'Azrouel dress variable',
                    price: '$65',
                    star: 5
                }
            ],
            product03: [
                {
                    img: '/image/product04.jpg',
                    title: 'amed-sleeve tops group',
                    price: '$60',
                    star: 3
                },{
                    img: '/image/product02.jpg',
                    title: 'Avitae lectus default',
                    price: '$61',
                    star: 5
                },{
                    img: '/image/product03.jpg',
                    title: 'Azrouel dress variable',
                    price: '$65',
                    star: 5
                },{
                    img: '/image/product01.jpg',
                    title: 'Azrouel dress variable',
                    price: '$65',
                    star: 5
                }
            ],
        };  
    }

    componentDidMount() {

    }

    render() {
        var product01 = this.state.product01;
        var product02 = this.state.product02;
        var product03 = this.state.product03;
        return(
            <div className="container">
                <div className="row">
                    <Tabs>
                        <TabList>
                            <Tab>best seller</Tab>
                            <Tab>new arrival</Tab>
                            <Tab>most wanter</Tab>
                        </TabList>

                        <TabPanel>
                            <Product product = {product01} />
                        </TabPanel>
                        <TabPanel>
                            <Product product = {product02} />
                        </TabPanel>
                        <TabPanel>
                            <Product product = {product03} />
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default TabProduct;