import React, {Component} from 'react';

class Product extends Component {
	constructor(props) {
        super(props);

        this.state = {
        };  
    }

    componentDidMount() {

    }

    render() {    	
    	const products = this.props.product;  	
    	const elmProduct = products.map((product, index) => {
    		return(    			
    			<div key={index} className="c-list01__item">
                    <div className="c-list01__img"><img src={product.img} alt="" /></div>
                    <div className="c-list01__text">
                        <h3 className="c-list01__ttl">{product.title}</h3>
                        <div className="c-list01__price">{product.price}</div>                        
                        <div className="c-list01__star">
		    				<i className="fa fa-star"></i>
		    				<i className="fa fa-star-o"></i>
		    				<i className="fa fa-star-o"></i>
		    				<i className="fa fa-star-o"></i>
		    				<i className="fa fa-star-o"></i>
	    				</div>
                    </div>
                    <div className="c-list01__btn">
                        <a href="#" className="c-list01__view is-hover03"><i className="fa fa-search"></i></a>
                        <button className="c-list01__addcart is-hover03"><i className="fa fa-cart-plus"></i></button>
                        <button className="c-list01__addwi is-hover03"><i className="fa fa-heart-o"></i></button>
                    </div>
                </div>
    		);
    	});
    	console.log(products);
        return(
        	<div className="c-list01">
                {elmProduct}
            </div>
        );
    }
}

export default Product;