import React, {Component} from 'react';

class SlideItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }
    render() {
        const item = this.props.sliderItem;
        return(  
            <div className="c-slider__item">
                <div className="c-slider__progress"></div>
                <div className="c-slider__img"><img src={item.img} alt="" /></div>
                <div className="c-slider__text">
                    <div className="container">
                        <div className="row">                    
                            <div className="c-slider__ttl01">{item.title}</div>
                            <div className="c-slider__ttl02">{item.share}</div>
                            <p className="c-slider__txt">{item.content}</p>
                            <a className="c-slider__btn" href={item.url}><span>Shop Now !</span></a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SlideItem;