import React, {Component} from 'react';

class Banner3col extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };  
    }

    componentDidMount() {
    	$(window).on('load resize', function () {
    		var h = $('.c-banner3col__item').width();
	        $('.c-banner3col__item:first').height(h);
	        $('.c-banner3col__item:not(:first)').height((h/2) - 15);
    	});        
    }

    render() {
        return(
        	<div className="container">
                <div className="row">
                    <div className="c-banner3col">
                        <div className="c-banner3col__item is-hover03">
                            <div className="c-banner3col__img"><img src={process.env.PUBLIC_URL + '/image/banner01.jpg'} alt="" /></div>
                            <div className="c-banner3col__text">
                            	<h3 className="c-banner3col__ttl">Title</h3>
                            	<p className="c-banner3col__txt">Content</p>
                            </div>
                        </div>
                        <div className="c-banner3col__item is-hover02">
                            <div className="c-banner3col__img"><img src={process.env.PUBLIC_URL + '/image/banner02.jpg'} alt="" /></div>
                            <div className="c-banner3col__text">
                            	<h3 className="c-banner3col__ttl">Title</h3>
                            	<p className="c-banner3col__txt">Content</p>
                            </div>
                        </div>
                        <div className="c-banner3col__item is-hover01">
                            <div className="c-banner3col__img"><img src={process.env.PUBLIC_URL + '/image/banner03.jpg'} alt="" /></div>
                            <div className="c-banner3col__text">
                            	<h3 className="c-banner3col__ttl">Title</h3>
                            	<p className="c-banner3col__txt">Content</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Banner3col;